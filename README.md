# Stage opdrachten - MBO Niveau 4
Er zijn 2 verschillende opdrachten die je moet uitwerken. Opdracht 1 is een kleine PHP opdracht en opdracht 2 is een design die je moet namaken.

Voeg de folders toe aan je lokale web development omgeving zoals Mamp, Wamp of Xampp om PHP te kunnen uitvoeren.

In de opdracht folders staat een aparte uitleg per opdracht die ook hieronder staat beschreven


## Opdracht 1
De volgende opdracht bestaat uit 2 delen. Het 1e deel is verplicht om te voltooien het 2e deel is optioneel om te laten zien dat je ook wat uitgebreidere logica kan schrijven.

### Deel 1
Je gaat een klein feest geven die een weekend duurt.
Hiervoor zijn aanmeldingen binnen gekomen. Deze zijn te vinden in het bestand submissions.json
Het enige nadeel is dat er aanmeldingen zijn door minderjarigen. en het feest is 18+. 

Maak in PHP een oplossing waarbij je de personen onder de 18 uit het JSON bestand filtert. De aanmelding staan in submissions.json Toon het JSON resultaat in de browser. Het resultaat moet er als volgt uitzien:

```
[
  "Sharon van vliet",
  "Pieter Kuijpers",
  "Pieter Hanke",
  "Adriana Jordaan",
  "Lydia Flip",
  "Nina Rosa"
]
```
Maak hiervoor een bestand aan genaamd solution-1.php

### Deel 2 (optioneel)
Het is nu ook nog onduidelijk op welke dag de personen aanwezig zullen zijn. Het feest duurt namelijk meerdere dagen. Groepeer daarom de namen op de verschillende datums. Gebruik de datum dan als key in de associatieve array en dan een array met de namen als value.
Het resultaat moet er als volgt uit zien:
```
{
  "2022-08-12": [
    "Sharon van vliet",
    "Pieter Hanke"
  ],
  "2022-08-13": [
    "Pieter Kuijpers",
    "Nina Rosa"
  ],
  "2022-08-14": [
    "Adriana Jordaan",
    "Lydia Flip"
  ]
}
```
Maak hiervoor een bestand aan genaamd solution-2.php


## Opdracht 2
In deze opdracht staat er een design. Deze moet je namaken in HTML + SCSS. Je kan een simpel HTML bestand aanmaken waarin je deze maakt of je deelt het op in PHP bestanden. Wat het makkelijkste voor je werkt.

We verwachten dat je minimaal de volgende dingen gebruikt in je code:

- [ ] SCSS die compiled naar CSS
- [ ] Grid systeem zoals Bootstrap

Je zou het Sketch bestand kunnen openen in Sketch op Mac of in Lunacy op Windows. De fonts en afbeeldingen die gebruikt worden zijn te vinden in de desbetreffende mappen.
Zorg dat de website ook goed responsive werkt! Dit is heel belangrijk in webdevelopment. Zet daarbij bijvoorbeeld de tekst en afbeeldingen onder elkaar.

Maak een losse map met de naam solution waarin je de uitwerking zet.

### Extra uitdaging
Je kan ook de sectie Blogberichten omzetten naar een Swiper. Hiermee kan je makkelijker schalen voor mobile. Je kan dan bijvoorbeeld 2 blogberichten laten zien op mobile en dat je via een slider door de blogberichten kan gaan. Je kan dit ook toepassen op desktop dat je dan 5 berichten hebt en dat je naar rechts kan sliden door pagination of een scrollbar of met knoppen.

Swiper documentatie:

https://swiperjs.com/get-started

https://swiperjs.com/swiper-api

Je zou voor extra functionaliteit bepaalde JS / jQuery scripts kunnen uitvoeren. Misschien dat je  de verschillende secties zoals de paginalinks in kan laten faden door een opacity/transition of een FadeIn.

Voorbeeld:

https://nls2in-hezarehha.savviihq.com/

