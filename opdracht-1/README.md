# Opdracht 1
De volgende opdracht bestaat uit 2 delen. Het 1e deel is verplicht om te voltooien het 2e deel is optioneel om te laten zien dat je ook wat uitgebreidere logica kan schrijven.

## Deel 1
Je gaat een klein feest geven die een weekend duurt.
Hiervoor zijn aanmeldingen binnen gekomen. Deze zijn te vinden in het bestand submissions.json
Het enige nadeel is dat er aanmeldingen zijn door minderjarigen. en het feest is 18+. 

Maak in PHP een oplossing waarbij je de personen onder de 18 uit het JSON bestand filtert. De aanmelding staan in submissions.json Toon het JSON resultaat in de browser. Het resultaat moet er als volgt uitzien:

```
[
  "Sharon van vliet",
  "Pieter Kuijpers",
  "Pieter Hanke",
  "Adriana Jordaan",
  "Lydia Flip",
  "Nina Rosa"
]
```
Maak hiervoor een bestand aan genaamd solution-1.php

## Deel 2
Het is nu ook nog onduidelijk op welke dag de personen aanwezig zullen zijn. Het feest duurt namelijk meerdere dagen. Groepeer daarom de namen op de verschillende datums. Gebruik de datum dan als key in de associatieve array en dan een array met de namen als value.
Het resultaat moet er als volgt uit zien:
```
{
  "2022-08-12": [
    "Sharon van vliet",
    "Pieter Hanke"
  ],
  "2022-08-13": [
    "Pieter Kuijpers",
    "Nina Rosa"
  ],
  "2022-08-14": [
    "Adriana Jordaan",
    "Lydia Flip"
  ]
}
```

Maak hiervoor een bestand aan genaamd solution-2.php